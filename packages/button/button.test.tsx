import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import Button from './button';

const tooltip = {
  default: 'Ignites the fuel',
  progress: 'Cancel Launch',
  error: 'Ignition Error'
};

test('Displays the default button', () => {
  const ButtonText = 'Launch Rocket';
  render(<Button name="button-test">{ButtonText}</Button>);
  expect(screen.getByText(ButtonText)).toBeInTheDocument();
});

test('Has tooltips when defined', () => {
  const ButtonText = 'Launch Rocket with Tooltip';
  const ButtonTextWO = 'Launch Rocket without Tooltip';
  render(
    <Button name="button-test" toolTip={tooltip}>
      {ButtonText}
    </Button>
  );
  render(<Button name="button-test-wo">{ButtonTextWO}</Button>);
  expect(screen.getByText(ButtonText)).toHaveAccessibleDescription();
  expect(screen.getByText(ButtonTextWO)).not.toHaveAccessibleDescription();
});

test('Renders a disabled button', () => {
  const ButtonText = 'Launch Rocket';
  render(
    <Button name="button-test" disabled>
      {ButtonText}
    </Button>
  );
  expect(screen.getByText(ButtonText)).toBeDisabled();
});

test('Change state by using props', () => {
  const ButtonTextProgress = 'Launch Rocket Progress';
  const ButtonTextError = 'Launch Rocket Error';
  render(
    <Button name="button-test-progress" initialState={'progress'}>
      {ButtonTextProgress}
    </Button>
  );
  render(
    <Button name="button-test-error" initialState={'error'}>
      {ButtonTextError}
    </Button>
  );
  expect(screen.getByText(ButtonTextProgress)).toHaveClass(
    'wfds-button-progress'
  );
  expect(screen.getByText(ButtonTextError)).toHaveClass('wfds-button-error');
});

test('Always display tooltip on error', () => {
  const ButtonText = 'Launch Rocket';
  render(
    <Button name="button-test-error" initialState={'error'} toolTip={tooltip}>
      {ButtonText}
    </Button>
  );
  expect(screen.getByText(ButtonText)).toHaveClass('wfds-tooltip-sticky');
  expect(screen.getByText(ButtonText)).toHaveAccessibleDescription();
});

test('Makes a network request when clicking on the button', async () => {
  const ButtonText = 'Launch Rocket';
  render(
    <Button
      name="button-test"
      toolTip={tooltip}
      url="https://httpbin.org/delay/5"
    >
      {ButtonText}
    </Button>
  );
  fireEvent.click(screen.getByText(ButtonText));
  await waitFor(() => {
    expect(screen.getByText(ButtonText)).toHaveClass('wfds-button-progress');
  });
});

test('Double click cancels the network request', async () => {
  const ButtonText = 'Launch Rocket';
  render(
    <Button
      name="button-test"
      toolTip={tooltip}
      url="https://httpbin.org/delay/5"
    >
      {ButtonText}
    </Button>
  );
  fireEvent.click(screen.getByText(ButtonText));
  fireEvent.click(screen.getByText(ButtonText));
  await waitFor(() => {
    expect(screen.getByText(ButtonText)).toHaveClass('wfds-button-error');
  });
});

test('Third click restarts the cancelled network request', async () => {
  const ButtonText = 'Launch Rocket';
  render(
    <Button
      name="button-test"
      toolTip={tooltip}
      url="https://httpbin.org/delay/5"
    >
      {ButtonText}
    </Button>
  );
  fireEvent.click(screen.getByText(ButtonText));
  fireEvent.click(screen.getByText(ButtonText));
  fireEvent.click(screen.getByText(ButtonText));
  await waitFor(() => {
    expect(screen.getByText(ButtonText)).toHaveClass('wfds-button-progress');
  });
});
